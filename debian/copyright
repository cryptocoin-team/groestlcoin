Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Groestlcoin Core Wallet
Upstream-Contact: https://github.com/GroestlCoin/groestlcoin/issues
Source: https://github.com/GroestlCoin/groestlcoin
 .
 Repackaged, excluding non-DFSG files, embedded code copies,
 an unsafe symlink, and a watch file confusing uscan.
Files-Excluded:
 foreign
 src/leveldb
 src/secp256k1
 src/univalue

Files: *
Copyright:
  2012-2014,2016-2017,2019-2021, Pieter Wuille
  2009-2010, Satoshi Nakamoto
  2009-2021, The Bitcoin Core developers
  2009-2012, The Bitcoin developers
  2014-2021, The Groestlcoin developers
  2017, The Zcash developers
  2014, Wladimir J. van der Laan
License-Grant:
 Distributed under the MIT software license,
 see the accompanying file <COPYING>
 or <http://www.opensource.org/licenses/mit-license.php>.
License: Expat
Comment:
 Referenced file and URL both contain
 the Expat flavor of MIT licenses.

Files:
 src/crc32c/LICENSE
Copyright:
  2008,2017, The CRC32C Authors
License: BSD-3-clause~Google

Files:
 contrib/macdeploy/*
Copyright:
  2011, Patrick "p2k" Schneider <me@p2k-network.org>
License: GPL-3+

Files:
 src/sphlib/*
Copyright:
  2007-2010, Projet RNRT SAPHIR
License: Expat

Files:
 build-aux/m4/ax_check_*_flag.m4
 build-aux/m4/ax_pthread.m4
Copyright:
  2008, Guido U. Draheim <guidod@gmx.de>
  2011, Maarten Bosmans <mkbosmans@gmail.com>
  2019, Marc Stevens <marc.stevens@cwi.nl>
License-Grant:
 This program is free software:
 you can redistribute it and/or modify it
 under the terms of the GNU General Public License
 as published by the Free Software Foundation,
 either version 3 of the License,
 or (at your option) any later version.
License: GPL-3+ with Autoconf-Macro exception
 As a special exception,
 the respective Autoconf Macro's copyright owner
 gives unlimited permission
 to copy, distribute and modify the configure scripts
 that are the output of Autoconf when processing the Macro.
 You need not follow the terms of the GNU General Public License
 when using or distributing such scripts,
 even though portions of the text of the Macro appear in them.
 The GNU General Public License (GPL) does govern
 all other use of the material that constitutes the Autoconf Macro.
 .
 This special exception to the GPL applies
 to versions of the Autoconf Macro
 released by the Autoconf Archive.
 When you make and distribute a modified version
 of the Autoconf Macro,
 you may extend this special exception
 to the GPL to apply to your modified version as well.
License-Reference: /usr/share/common-licenses/GPL-3

Files:
 src/el/*
Copyright:
  None (in the public domain)
License: public-domain
 Files
  el/bignum/x86x64/bignum-x86x64.asm.
  el/bignum/x86x64/montgomery_mul_x86x64.asm.
 are placed in the Public Domain
Comment:
 Same license is assumed applies also to src/el/x86x64.inc.
 .
 See <https://github.com/GroestlCoin/groestlcoin/issues/3>

Files:
 src/bench/nanobench.h
Copyright:
  2019-2020, Martin Ankerl <martin.ankerl@gmail.com>
License: Expat

Files:
 depends/config.guess
 depends/config.sub
Copyright:
  1992-2015, Free Software Foundation, Inc.
License-Grant:
 This file is free software;
 you can redistribute it and/or modify it
 under the terms of the GNU General Public License
 as published by the Free Software Foundation;
 either version 3 of the License, or (at your option) any later version.
License: GPL-3 with Autoconf exception
 As a special exception to the GNU General Public License,
 if you distribute this file as part of a program
 that contains a configuration script generated by Autoconf,
 you may include it under the same distribution terms
 that you use for the rest of that program.
 This Exception is an additional permission
 under section 7 of the GNU General Public License, version 3
 ("GPLv3").
License-Reference: /usr/share/common-licenses/GPL-3

Files:
 build-aux/m4/ax_boost_*
 build-aux/m4/ax_gcc_func_attribute.m4
Copyright:
  2008, Daniel Casimiro <dan.casimiro@gmail.com>
  2013, Gabriele Svelto <gabriele.svelto@gmail.com>
  2008-2009, Michael Tindal
  2009, Peter Adolphs
  2009, Roman Rybalko <libtorrent@romanr.info>
  2008-2009, Thomas Porschberg <thomas@randspringer.de>
  2012, Xiyue Deng <manphiz@gmail.com>
License: FSFULLR~NotFSF

Files:
 src/tinyformat.h
Copyright:
  2011, Chris Foster <chris42f@gmail.com>
License: BSL-1

Files:
 src/crypto/sha256_sse4.cpp
Copyright:
  2012, Intel Corporation
  2017, The Bitcoin Core developers
License-Grant:
 Distributed under the MIT software license,
 see the accompanying file <COPYING>
 or <http://www.opensource.org/licenses/mit-license.php>.
License: Expat and BSD-3-Clause~Intel
Comment:
 Referenced file and URL both contain
 the Expat flavor of MIT licenses.

Files:
 build-aux/m4/ax_pthread.m4
Copyright:
  2011, Daniel Richard G. <skunk@iSKUNK.ORG>
  2008, Steven G. Johnson <stevenj@alum.mit.edu>
License-Grant:
 This program is free software:
 you can redistribute it and/or modify it
 under the terms of the GNU General Public License
 as published by the Free Software Foundation,
 either version 3 of the License,
 or (at your option) any later version.
License: GPL-3+ with Autoconf Archive exception
 As a special exception,
 the respective Autoconf Macro's copyright owner
 gives unlimited permission
 to copy, distribute and modify the configure scripts
 that are the output of Autoconf when processing the Macro.
 You need not follow the terms of the GNU General Public License
 when using or distributing such scripts,
 even though portions of the text of the Macro appear in them.
 The GNU General Public License (GPL)
 does govern all other use of the material
 that constitutes the Autoconf Macro.
 .
 This special exception to the GPL applies
 to versions of the Autoconf Macro
 released by the Autoconf Archive.
 When you make and distribute a modified version
 of the Autoconf Macro,
 you may extend this special exception to the GPL
 to apply to your modified version as well.
License-Reference: /usr/share/common-licenses/GPL-3

Files:
 contrib/testgen/base58.py
Copyright:
  None (in the public domain)
License: public-domain
 Based on <https://bitcointalk.org/index.php?topic=1026.0>
 (public domain)
Comment:
 At above referenced URL, Gavin Andresen states
 "I hereby release this code into the public domain,
 do with it what you will.
 And please let me know if you find any bugs in it."

Files:
 test/lint/devtools/git-subtree-check.sh
Copyright:
  2009, Avery Pennarun <apenwarr@gmail.com>
License-Grant:
 Taken from git-subtree
License: GPL-2
Comment:
 git-subtree is part of git, which is licensed GPL-2.

Files:
 src/qt/res/icons/*.png
 src/qt/res/src/*.svg
Copyright:
 None (in the public domain)
License: public-domain
 Copyright: No copyright holder, these files are put in public domain
 .
 License: public-domain
 .
 Comment: The author, Utopian, declares the source code to be
 put in the public domain
Comment:
 Copyright and license is documented in <doc/assets-attribution.md>.

Files:
 test/functional/test_framework/authproxy.py
Copyright:
  2007, Jan-Klaas Kollhof
  2011, Jeff Garzik
  from python-jsonrpc/jsonrpc/proxy.py:
License: LGPL-2.1+

Files:
 debian/*
Copyright:
  2016-2021, Jonas Smedegaard <dr@jones.dk>
  2020, Purism, SPC
License: GPL-3+

Files:
 debian/patches/1001*.patch
Copyright:
  Dmitry Smirnov <onlyjob@member.fsf.org>
  Jonas Smedegaard <dr@jones.dk>
License: GPL-2+

Files:
 debian/patches/1002*.patch
Copyright:
  Jonas Smedegaard <dr@jones.dk>
License: Expat

License: Expat
 Permission is hereby granted, free of charge,
 to any person obtaining a copy
 of this software and associated documentation files
 (the "Software"),
 to deal in the Software without restriction,
 including without limitation
 the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software,
 and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright notice and this permission notice
 shall be included
 in all copies or substantial portions of the Software.

License: BSD-3-Clause~Intel
 Redistribution and use in source and binary forms,
 with or without modification,
 are permitted provided that the following conditions are met:
 .
  * Redistributions of source code must retain
    the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce
    the above copyright notice,
    this list of conditions and the following disclaimer
    in the documentation and/or other materials
    provided with the distribution.
  * Neither the name of the Intel Corporation
    nor the names of its contributors
    may be used to endorse or promote
    products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY INTEL CORPORATION "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.
 IN NO EVENT SHALL INTEL CORPORATION OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-3-clause~Google
 Redistribution and use in source and binary forms,
 with or without modification,
 are permitted
 provided that the following conditions are met:
   * Redistributions of source code must retain
     the above copyright notice, this list of conditions
     and the following disclaimer.
   * Redistributions in binary form must reproduce
     the above copyright notice, this list of conditions
     and the following disclaimer
     in the documentation and/or other materials
     provided with the distribution.
   * Neither the name of Google Inc.
     nor the names of its contributors
     may be used to endorse or promote
     products derived from this software
     without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED
 BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSL-1
 Permission is hereby granted, free of charge,
 to any person or organization obtaining a copy
 of the software and accompanying documentation
 covered by this license (the "Software")
 to use, reproduce, display, distribute, execute,
 and transmit the Software,
 and to prepare derivative works of the Software,
 and to permit third-parties to whom the Software is furnished to do so,
 all subject to the following:
 .
 The copyright notices in the Software and this entire statement,
 including the above license grant, this restriction
 and the following disclaimer,
 must be included in all copies of the Software,
 in whole or in part,
 and all derivative works of the Software,
 unless such copies or derivative works are solely
 in the form of machine-executable object code
 generated by a source language processor.
 .
 THE SOFTWARE IS PROVIDED "AS IS",
 WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO
 THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE,
 TITLE AND NON-INFRINGEMENT.
 IN NO EVENT SHALL THE COPYRIGHT HOLDERS
 OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 FOR ANY DAMAGES OR OTHER LIABILITY,
 WHETHER IN CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: FSFULLR~NotFSF
 Copying and distribution of this file,
 with or without modification,
 are permitted in any medium without royalty
 provided the copyright notice and this notice are preserved.
 This file is offered as-is,
 without any warranty.

License: GPL-2
License-Reference: /usr/share/common-licenses/GPL-2

License: GPL-2+
License-Reference: /usr/share/common-licenses/GPL-2

License: GPL-3+
License-Reference: /usr/share/common-licenses/GPL-3

License: LGPL-2.1+
License-Reference: /usr/share/common-licenses/LGPL-2.1+
